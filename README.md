# MDB #

# Stack

* Node
* Mongodb
* Express
* EJS

### What is this repository for? ###

* Node Project for scaffolding and templating future node projects
* Also used to try new node technologies.

### How do I get set up? ###

* Install node and npm
* Install and set up mongodb as a service.
* Install a Code editor of choice
* run npm install to update all modules
* npm test to test the application
* npm start to run the application.
