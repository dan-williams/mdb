'use strict';

(function (hasher) {
  var crypto = require('crypto')

  hasher.computeHash = function (source) {
    var hmac = crypto.createHmac('sha1', 'W&S3c0yvQ@4]')
    var hash = hmac.update(source)

    return hash.digest('hex')
  }
})(module.exports)
