"use strict";

(function(auth) {
    var user = require("../models/userSchema");
    var hasher = require("./hasher");
    var passport = require("passport");
    var LocalStrategy = require("passport-local").Strategy;
    var selectFilter = "-__v";

    var loginError = function(messageString, next) { next(null, false, { error: messageString }); };
    var credentialLoginError = function(next) { loginError("Invalid Credentials.", next); };

    function userVerify(username, password, next) {
        user.findOne({ username: username }, selectFilter + " +hashedPassword", function(err, user) {
            if(err || user === null) {
                credentialLoginError(next);
            } else {
                var testHash = hasher.computeHash(password);
                if(testHash === user.hashedPassword) {
                    next(null, user);
                } else {
                    credentialLoginError(next);
                }
            }
        });
    }

    auth.ensureAuthenticated = function(req, res, next) {
        if(req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send("Unauthorised access");
        }
    };

    auth.init = function(app) {
        passport.use(new LocalStrategy(userVerify));
        passport.serializeUser(function(user, next) {
            next(null, user.username);
        });

        passport.deserializeUser(function(key, next) {
            user.findOne({ username: key }, function(err, user) {
                if(err || user === null) {
                    loginError("Failed to retrieve user", next);
                } else {
                    next(null, user);
                }
            });
        });

        app.use(passport.initialize());
        app.use(passport.session());
    };
})(module.exports);
