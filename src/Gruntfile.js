module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', './*.js'],
      options: {
        globals: {
          console: true
        }
      }
    },
    watch: {
      files: ['.jshintrc'],
      tasks: ['jshint']
    }
  })

  grunt.loadNpmTasks('grunt-contrib-jshint')
  grunt.loadNpmTasks('grunt-contrib-watch')

  grunt.registerTask('default', ['jshint'])
}
