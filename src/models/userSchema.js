'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var userSchema = new Schema({
  name: String,
  username: { type: String, required: true, unique: true},
  password: { type: String, required: true},
  role: {type: String, required: true},

  created: {type: Date, default: Date.now},
  lastModified: {type: Date, default: Date.now}
})

var User = mongoose.model('User', userSchema)

module.exports = User
