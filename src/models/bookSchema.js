'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var bookSchema = new Schema({
  name: String,
  author: { type: String, required: true},
  genre: { type: String},
  description: { type: String, required: true},

  created: {type: Date, default: Date.now},
  lastModified: {type: Date, default: Date.now}
})

var Book = mongoose.model('Book', bookSchema)

module.exports = Book
