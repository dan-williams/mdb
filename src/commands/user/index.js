'use strict'

;(function (userCommands) {
  var userCreate = require('./create.js')
  var userCreateCommand = new userCreate()
  var userDelete = require('./delete.js')
  var userDeleteCommand = new userDelete()
  var userUpdate = require('./update.js')
  var userUpdateCommand = new userUpdate()
  var userRead = require('./read.js')
  var userReadCommand = new userRead()
  var userUpdatePassword = require('./updatePassword.js')
  var userUpdatePasswordCommand = new userUpdatePassword()

  userCommands.Create = function (Username, Password, Name, Role) {
    userCreateCommand.Execute(Username, Password, Name, Role)
  }
  userCommands.Update = function (Username, Password, Name, Role) {
    userUpdateCommand.Execute(Username, Password, Name, Role)
  }

  userCommands.UpdatePassword = function (Username, Password) {
    userUpdatePasswordCommand.Execute(Username, Password)
  }

  userCommands.CanRead = function () {
    return userReadCommand.Authorised
  }

  userCommands.CanDelete = function () {
    return userDeleteCommand.Authorised
  }

  userCommands.CanCreate = function () {
    return userCreateCommand.Authorised
  }

  userCommands.CanUpdate = function () {
    return userUpdateCommand.Authorised
  }

  userCommands.CanUpdatePassword = function () {
    return userUpdatePasswordCommand.Authorised
  }

userCommands.IsAdmin = function (user) {
    return user.role === 'Admin'
  }

})(module.exports)
