'use strict'

var user = require('../../models/userSchema')
var hash = require('../../auth/hasher')

function UserUpdatePasswordCommand () {
}

UserUpdatePasswordCommand.prototype.Execute = function (Username, Password) {
  user.findOne({username: Username}, function (err, user) {
    user.password = hash.computeHash(Password)
    user.save()
  })
}

UserUpdatePasswordCommand.prototype.Authorised = function () {
    return true;
}

module.exports = UserUpdatePasswordCommand
