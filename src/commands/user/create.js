'use strict'

var user = require('../../models/userSchema')
var hash = require('../../auth/hasher')
var log = require('../../common/logger.js')
var Logger = new log()

function UserCreateCommand () {
}

UserCreateCommand.prototype.Execute = function (Username, Password, Name, Role) {
 Logger.Info('creating user: '+ Username)
  var userObject = new user({
    username: Username,
    password: hash.computeHash(Password),
    name: Name,
    role: Role
  })
  userObject.save(function (err) {
    Logger.Error(err)
  })
}

UserCreateCommand.prototype.Authorised = function () {
    return true;
}

module.exports = UserCreateCommand
