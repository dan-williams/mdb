'use strict'

var user = require('../../models/userSchema')
var hash = require('../../auth/hasher')

function UserUpdateCommand () {
}

UserUpdateCommand.prototype.Execute = function (Username, Name, Role) {
  user.findOne({username: Username}, function (err, user) {
    user.username = Username
    user.name = Name
    user.role = Role
    user.save()
  })
}

UserUpdateCommand.prototype.Authorised = function () {
    return true;
}

module.exports = UserUpdateCommand
