"use strict";

(function(reportCommands) {
    var bookExport = require("./bookExport.js");
    var cmdBookExport = new bookExport();
    var userExport = require("./userExport.js");
    var cmdUserExport = new userExport();
    var statsExport = require("./statsExport.js");
    var cmdStatsExport = new statsExport();

    reportCommands.ExportBook = function(res){
        cmdBookExport.Execute(res)
    }

    reportCommands.ExportUser = function(res){
        cmdUserExport.Execute(res)
    }

    reportCommands.ExportStats = function(res){
        cmdStatsExport.Execute(res)
    }

})(module.exports);
