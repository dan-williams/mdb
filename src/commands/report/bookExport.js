'use strict'

var fileExport = require('../../common/export')
var book = require('../../models/bookSchema')
var log = require('../../common/logger.js')
var Logger = new log()

function BookExportCommand () {
}

BookExportCommand.prototype.Execute = function (res) {
  book.find({}, function (err, found) {
    var cols = [
      {
        caption: 'Name',
        type: 'string'
      },
      {
        caption: 'Author',
        type: 'string',
      },
      {
        caption: 'Genre',
        type: 'string'
      },
      {
        caption: 'Description',
        type: 'string'
      },
      {
        caption: 'Last Modified',
        type: 'date'
      },
      {
        caption: 'Created',
        trype: 'date'
      }
    ]

    var rows = []

    found.forEach(function (book) {
      rows.push([book.name, book.author, book.genre, book.description, book.lastModified, book.created])
    })

    fileExport.Excel(res, cols, rows)

    if (err) {
      Logger.Error(err)
    } else {
      Logger.Info('Genrating Book Report Excel Document')
    }
  })
}

module.exports = BookExportCommand
