'use strict'

var fileExport = require('../../common/export')
var stats = require('../../common/analytics/statisticsSchema.js')
var log = require('../../common/logger.js')
var Logger = new log()

function StatsExportCommand () {
}

StatsExportCommand.prototype.Execute = function (res) {
  stats.find({}, function (err, found) {
    var cols = [
      {
        caption: 'Route',
        type: 'string'
      },
      {
        caption: 'Request',
        type: 'string',
      },
      {
        caption: 'Username',
        type: 'string'
      },
      {
        caption: 'Date Requested',
        type: 'date'
      }
    ]

    var rows = []

    found.forEach(function (data) {
      rows.push([data.route, data.request, data.user_username, data.dateRequested])
    })

    fileExport.Excel(res, cols, rows)

    if (err) {
      Logger.Error(err)
    } else {
      Logger.Info('Genrating Statistics Report Excel Document')
    }
  })
}

module.exports = StatsExportCommand
