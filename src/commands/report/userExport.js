'use strict'

var fileExport = require('../../common/export')
var user = require('../../models/userSchema')
var log = require('../../common/logger.js')
var Logger = new log()

function UserExportCommand () {
}

UserExportCommand.prototype.Execute = function (res) {
  user.find({}, function (err, found) {
    var cols = [
      {
        caption: 'Username',
        type: 'string'
      },
      {
        caption: 'Name',
        type: 'string',
      },
      {
        caption: 'Role',
        type: 'string'
      },
      {
        caption: 'Last Modified',
        type: 'date'
      },
      {
        caption: 'Created',
        trype: 'date'
      }
    ]

    var rows = []

    found.forEach(function (data) {
      rows.push([data.username, data.name, data.role, data.lastModified, data.created])
    })

    fileExport.Excel(res, cols, rows)

    if (err) {
      Logger.Error(err)
    } else {
      Logger.Info('Genrating User Report Excel Document')
    }
  })
}

module.exports = UserExportCommand
