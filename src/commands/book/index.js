"use strict";

(function (bookCommands) {
  var bookCreate = require('./create.js')
  var bookCreateCommand = new bookCreate()
  var bookDelete = require('./delete.js')
  var bookDeleteCommand = new bookDelete()
  var bookUpdate = require('./update.js')
  var bookUpdateCommand = new bookUpdate()
  var bookRead = require('./read.js')
  var bookReadCommand = new bookRead()

  bookCommands.Create = function (Name, Author, Genre, Description) {
    bookCreateCommand.Execute(Name, Author, Genre, Description)
  }
  bookCommands.Update = function (Name, Author, Genre, Description) {
    bookUpdateCommand.Execute(Name, Author, Genre, Description)
  }

  bookCommands.CanRead = function () {
    return bookReadCommand.Authorised
  }

  bookCommands.CanDelete = function () {
    return bookDeleteCommand.Authorised
  }

  bookCommands.CanCreate = function () {
    return bookCreateCommand.Authorised
  }

  bookCommands.CanUpdate = function () {
    return bookUpdateCommand.Authorised
  }
})(module.exports)
