'use strict'

var book = require('../../models/bookSchema')

function BookUpdateCommand () {
}

BookUpdateCommand.prototype.Execute = function (Name, Author, Genre, Description) {
  book.findOne({name: Name}, function (err, book) {
    book.name = Name
    book.author = Author
    book.genre = Genre
    book.description = Description
    book.save()
  })
}

BookUpdateCommand.prototype.Authorised = function () {
    return true;
}

module.exports = BookUpdateCommand
