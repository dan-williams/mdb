'use strict'

var book = require('../../models/bookSchema')
var log = require('../../common/logger.js')
var Logger = new log()

function BookCreateCommand () {
}

BookCreateCommand.prototype.Execute = function (Name, Author, Genre, Description) {
  Logger.Info('creating book: ' + Name)
  var bookObject = new book({
    name: Name,
    author: Author,
    genre: Genre,
    description: Description
  })
  bookObject.save(function (err) {
    Logger.Error(err)
  })
}

BookCreateCommand.prototype.Authorised = function () {
    return true;
}

module.exports = BookCreateCommand
