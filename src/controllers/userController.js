'use strict'

var userModel = require('../models/userSchema')
var hash = require('../auth/hasher')
var userRoles = require('../enums/userRoles')
var UserCommands = require('../commands/user')
var log = require('../common/logger.js')
var Logger = new log()
var stats = require('../common/analytics')

module.exports = function (app, requireLogin, isAuthorised) {
  app.get('/users', requireLogin, function (req, res) {
    isAuthorised(req, res, UserCommands.CanRead)
    userModel.find({}, function (err, found) {
      res.render('../views/user/user.ejs', {users: found,
        title: 'Users',
      currentUser: req.session.user}
      )
    })
    stats.Add('/users', 'get', req.session.user)
  })

  app.get('/profile', requireLogin, function (req, res) {
    res.render('../views/user/profile.ejs', {title: req.session.user.name,
      username: req.session.user.name,
    currentUser: req.session.user}
    )
    stats.Add('/profile', 'get', req.session.user)
  })

  app.get('/users/update/:username', requireLogin, function (req, res) {
    isAuthorised(req, res, UserCommands.CanUpdate)

    userModel.findOne({username: req.params.username}, function (err, found) {
      res.render('../views/user/update.ejs', {
        user: found,
        message: req.query.message,
        roles: Object.keys(userRoles),
        title: 'Update',
      currentUser: req.session.user}
      )
    })
    stats.Add('/users/update/:username', 'get', req.session.user)
  })

  app.post('/users/update/:username', function (req, res) {
    if (req.body.password === req.body.confirm) {
      UserCommands.Update(
        req.body.username,
        req.body.name,
        (req.body.role) ? req.body.role : req.body.role.user)

      res.redirect('/users')
      stats.Add('/users/update/:username', 'post', req.session.user)
    } else {
      res.redirect('/users/update/' + req.params.username + '?message=passwords do not match')
      Logger.Error('Can not update user, Passwords do not match!')
    }
  })

  app.get('/users/update/password/:username', requireLogin, function (req, res) {
    isAuthorised(req, res, UserCommands.CanUpdatePassword)

    userModel.findOne({username: req.params.username}, function (err, found) {
      res.render('../views/user/updatePassword.ejs', {user: found,
        message: req.query.message,
        title: 'Update',
      currentUser: req.session.user}
      )
    })
    stats.Add('/users/update/password/:username', 'get', req.session.user)
  })

  app.post('/users/update/password/:username', function (req, res) {
    userModel.findOne({username: req.params.username}, function (err, found) {
      if (found.password === hash.computeHash(req.body.oldPassword)) {
        if (req.body.password === req.body.confirm) {
          UserCommands.UpdatePassword(
            req.params.username,
            req.body.password)

          res.redirect('/users')
          stats.Add('/users/update/password/:username', 'post', req.session.user)
        } else {
          res.redirect('/users/update/password/' + req.params.username + '?message=passwords do not match')
          Logger.Error('Can not update user, Passwords do not match!')
        }
      }
    })
  })

  app.get('/users/update/password/:username/admin', requireLogin, function (req, res) {
    isAuthorised(req, res, UserCommands.IsAdmin(req.session.user))

    userModel.findOne({username: req.params.username}, function (err, found) {
      res.render('../views/user/updatePasswordAsAdmin.ejs', {user: found,
        message: req.query.message,
        title: 'Update',
      currentUser: req.session.user}
      )
    })
    stats.Add('/users/update/password/:username/admin', 'get', req.session.user)
  })

  app.post('/users/update/password/:username/admin', function (req, res) {
    if (req.body.password === req.body.confirm) {
      UserCommands.UpdatePassword(
        req.params.username,
        req.body.password)

      res.redirect('/users')
      stats.Add('/users/update/password/:username/admin', 'post', req.session.user)
    } else {
      res.redirect('/users/update/password/' + req.params.username + '/admin?message=passwords do not match')
      Logger.Error('Can not update user, Passwords do not match!')
    }
  })

  app.get('/users/delete/:username', requireLogin, function (req, res) {
    isAuthorised(req, res, UserCommands.CanDelete)
    userModel.findOne({username: req.params.username}, function (err, user) {
      user.remove()
      res.redirect('/users')
      stats.Add('/users/delete/:username', 'get', req.session.user)
    })
  })

  app.get('/signup', function (req, res) {
    res.render('../views/user/signup.ejs' , {
      message: req.query.message,
      roles: Object.keys(userRoles),
    title: 'Sign up'})
    stats.Add('/signup', 'get', null)
  })

  app.post('/signup', function (req, res) {
    if (req.body.password === req.body.confirm) {
      CreateCommand.Execute(
        req.body.username,
        req.body.password,
        req.body.name,
        (req.body.role) ? req.body.role : req.body.role.user)

      res.redirect('/users')
      stats.Add('/signup', 'post', null)
    } else {
      Logger.Error('Can not create user, Passwords do not match!')
      res.redirect('/signup' + '?message=passwords do not match')
    }
  })

  app.get('/login', function (req, res) {
    res.render('../views/user/login.ejs' , {
      message: req.query.message,
    title: 'Login'})
  })

  app.post('/login', function (req, res) {
    userModel.findOne({ username: req.body.username }, function (err, result) {
      if (err) {
        Logger.Error(err)
      }

      if (!result) {
        Logger.Error('User ' + req.body.username + ' Does not exist , redirecting to login page!')
        res.render('../views/user/login.ejs', {
          error: 'Invalid username or password.',
        title: 'Login' })
      } else {
        if (hash.computeHash(req.body.password) === result.password) {
          Logger.Info(req.body.username + ': Succesful login!')
          req.session.user = result
          res.redirect('/')
        } else {
          Logger.Error('Invalid username or password')
          res.render('../views/user/login.ejs', {
            error: 'Invalid username or password.',
          title: 'Login' })
        }
      }
    })
  })

  app.get('/logout', function (req, res) {
    Logger.Info(req.body.username + 'Logged out!')
    req.session.reset()
    res.redirect('/')
  })
}
