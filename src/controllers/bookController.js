'use strict'

var book = require('../models/bookSchema')
var bookGenres = require('../enums/bookGenres')
var BookCommands = require('../commands/book')
var stats = require('../common/analytics')

module.exports = function (app, requireLogin, isAuthorised) {
  app.get('/books', requireLogin, function (req, res) {
    book.find({}, function (err, found) {
      res.render('../views/books/books.ejs' , {book: found,
        title: 'Book',
      currentUser: req.session.user})
      stats.Add('/books', 'get', req.session.user)
    })
  })

  app.get('/books/create', requireLogin, function (req, res) {
    isAuthorised(req, res, BookCommands.CanCreate);

    res.render('../views/books/createbooks.ejs' , {message: req.query.message,
      genres: Object.keys(bookGenres),
      title: 'Create Books',
    currentUser: req.session.user}
    )
    stats.Add('/books/create', 'get', req.session.user)
  })

  app.post('/books/create', function (req, res) {
    BookCommands.Create(
      req.body.name,
      req.body.author,
      req.body.genre,
      req.body.description)
    res.redirect('/books')
    stats.Add('/books/create', 'post', req.session.user)
  })

  app.get('/books/update/:name', requireLogin, function (req, res) {
    isAuthorised(req, res, BookCommands.CanUpdate)

    book.findOne({name: req.params.name}, function (err, found) {
      res.render('../views/books/bookupdate.ejs', {book: found,
        message: req.query.message,
        genres: Object.keys(bookGenres),
        title: 'Book Update',
      currentUser: req.session.user}
      )
    })
    stats.Add('/books/update/:name', 'get', req.session.user)
  })

  app.post('/books/update/:name', function (req, res) {
    BookCommands.Update(
      req.body.name,
      req.body.author,
      req.body.genre,
      req.body.description)
    res.redirect('/books')
    stats.Add('/books/update/:name', 'post', req.session.user)
  })

  app.get('/books/delete/:name', requireLogin, function (req, res) {
    isAuthorised(req, res, BookCommands.CanDelete)

    book.findOne({name: req.params.name}, function (err, book) {
      book.remove()
      res.redirect('/books')
      stats.Add('/books/delete/:name', 'get', req.session.user)
    })
  })
}
