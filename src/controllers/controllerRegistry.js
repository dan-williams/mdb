'use strict'

var log = require('../common/logger.js')
var Logger = new log()

module.exports = function (app) {
  function requireLogin (req, res, next) {
    if (!req.user) {
      res.redirect('/login')
    } else {
      next()
    }
  }

  function isAuthorised(req, res, canAction) {
    if (!canAction) {
      res.redirect('/')
      Logger.Warn("Not Authorised to Perform that action: " + req.user)
    }
  }

  require('./securityController.js')(app, requireLogin)
  require('./bookController.js')(app, requireLogin, isAuthorised)
  require('./userController.js')(app, requireLogin, isAuthorised)
  require('./reportController.js')(app, requireLogin, isAuthorised)

  app.get('*', function (req, res) {
    res.render('../views/common/404.ejs')
  })
}
