'use strict'

var stats = require('../common/analytics')

module.exports = function (app, requireLogin) {
  app.get('/', requireLogin, function (req, res) {
    res.render('../views/common/home.ejs', {title: 'Home', currentUser: req.session.user})
  })

  app.get('/cookiePolicy', function (req, res) {
    res.render('../views/Security/cookiePolicy.ejs' , {title: 'Cookie Policy'}
    )
    stats.Add('/cookiePolicy', 'get', req.session.user)
  })
}
