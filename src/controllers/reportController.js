'use strict'

var reportCommands = require('../commands/report')
var stats = require('../common/analytics')

module.exports = function (app, requireLogin, isAuthorised) {
  app.get('/reports/books', requireLogin, function (req, res) {
    if (req.user.role === 'Admin') {
      res.render('../views/reports/books.ejs', {title: 'Book Report', currentUser: req.session.user})
    } else {
      res.redirect('/')
    }
  })

  app.get('/reports/users', requireLogin, function (req, res) {
    if (req.user.role === 'Admin') {
      res.render('../views/reports/users.ejs', {title: 'User Report', currentUser: req.session.user})
    } else {
      res.redirect('/')
    }
  })

  app.get('/reports/stats', requireLogin, function (req, res) {
    if (req.user.role === 'Admin') {
      res.render('../views/reports/stats.ejs', {title: 'Statistics Report', currentUser: req.session.user})
    } else {
      res.redirect('/')
    }
  })

  app.get('/reports/stats/export', requireLogin, function (req, res) {
    reportCommands.ExportStats(res)
    stats.Add('/reports/stats/export', 'get', req.session.user)
  })

  app.get('/reports/users/export', requireLogin, function (req, res) {
    reportCommands.ExportUser(res)
    stats.Add('/reports/users/export', 'get', req.session.user)
  })

  app.get('/reports/books/export', requireLogin, function (req, res) {
    reportCommands.ExportBook(res)
    stats.Add('/reports/books/export', 'get', req.session.user)
  })
}
