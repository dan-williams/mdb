'use strict'

module.exports = Object.freeze({
  scifi: 'Sci-Fi',
  horror: 'Horror',
  crime: 'Crime',
  biography: 'Biography'

})
