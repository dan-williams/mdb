'use strict'

var express = require('express')
var app = express()
var https = require('https')
var http = require('http')
var fs = require('fs')
var bodyParser = require('body-parser')
var log = require('./common/logger.js')
var Logger = new log();

var sslOptions = {
  /* key: fs.readFileSync('./auth/ssl/cakey.pem'),
   cert: fs.readFileSync('./auth/ssl/cacert.pem'),
   requestCert: true,
   rejectUnauthorized: false*/
}

try {
  Logger.Info('Setting View Engine!')

  app.use('/views', express.static(__dirname + '/views'))
  app.use(express.static(__dirname + '/public'))
  app.use(bodyParser())
  app.set('view engine', 'ejs')
  
} catch (error) {
  Logger.Fatal(error);
}

var port = 3000

Logger.Info('Pulling in middleware and controllers')

require('./middleware/database.js')(app)
require('./middleware/session.js')(app)
require('./api/apiRegistry.js')(app)
require('./controllers/controllerRegistry.js')(app)

var server
var protocol

try {
  Logger.Info('Creating Server!');

  if (process.env.MODE === 'prod') {
    server = https.createServer(sslOptions, app)
    protocol = 'https'
  } else {
    server = http.createServer(app)
    protocol = 'http'
  }
} catch (error) {
  Logger.Fatal(error);
}

try {
  server.listen(port, 'localhost')
  Logger.Info('Listening to ' + protocol + '://localhost:' + port)
} catch(error) {
  Logger.Debug(error)
}
