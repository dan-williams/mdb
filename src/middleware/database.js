'use strict'

var mongoose = require('mongoose')
var pop = require('../common/populator.js')
var Populator = new pop()
var log = require('../common/logger.js')
var Logger = new log();

module.exports = function (app) {
  var connectionString

  if (process.env.MODE === 'Prod') {
    connectionString = 'na'
    Logger.Info('Connecting to Prod Database')
  } else {
    connectionString = 'mongodb://localhost/mdb'
    Logger.Info('Connecting to Dev Database')
  }
  if (connectionString === 'na') {
    Logger.Warn('There is no Prod Database yet!')
  } else {
    mongoose.connect(connectionString)
  }

  var db = mongoose.connection
  db.on('error', console.error.bind(console, 'database connection error:'))
  db.once('open', function () {
    Logger.Info('Connected to database')
    if(process.env.MODE !== 'Prod'){
      Logger.Info("Calling Populator!");
      Populator.Populate(db);
    }
  })
}
