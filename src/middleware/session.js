'use strict'

var session = require('client-sessions')
var user = require('../models/userSchema')

module.exports = function (app) {
  app.use(session({
    cookieName: 'session',
    secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    httpOnly: true,
    secure: false,
    ephemeral: true
  }))

  app.use(function (req, res, next) {
    if (req.session && req.session.user) {
      user.findOne({ username: req.session.user.username }, function (err, user) {
        if (user) {
          req.user = user
          delete req.user.password
          req.session.user = user
          res.locals.user = user
        }
        next()
      })
    } else {
      next()
    }
  })
}
