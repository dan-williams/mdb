describe("bookSchema", function() {
  var Book = require('../../models/bookSchema.js');
  var helpers = require('../helpers');

  beforeEach(function() {
    helpers.ClearSchema(Book);
  });


  it("should be able to save a book schema", function() {

    var expected = new Book({
      name: "bookName",
      author: "bookAuthor",
      genre: "horror",
      description: "bookDesc"
    });

    expected.save(function(err) {
      console.log(err)
    });

    var actual = Book.find({}, function(err, found) {
      return found;
    });

    expect(actual).toEqual(expected);

  });

});