describe("userSchema", function() {
    var User = require('../../models/userSchema.js');
    var cmd = require('../../commands/user');
    var helpers = require('../helpers');

    beforeEach(function() {
        helpers.ClearSchema(User);
    });

    it("should be able to save a user schema", function() {

        var expected = new User({
            username: "expected user",
            password: "Shhhh",
            name: "expect name",
            role: "Admin"
        });

        expected.save(function(err) {
            console.log(err)
        });

        var actual = User.find({}, function(err, found) {
            console.log(found);
            return found;
        });

        expect(actual).toEqual(expected);

    });
});