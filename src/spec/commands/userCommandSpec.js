describe("userCommands", function() {
    var User = require('../../models/userSchema.js');
    var cmd = require('../../commands/user');
    var helpers = require('../helpers');

    beforeEach(function() {
        helpers.ClearSchema(User);
    });

    it("should be able to create a user", function() {

        var expected = new User({
            username: "expected user",
            password: "Shhhh",
            name: "expect name",
            role: "Admin"
        });

        cmd.Create(expected.username,
            expected.password,
            expected.name,
            expected.role);

        var actual = User.find({}, function(err, found) {
            console.log(found);
            return found;
        });

        expect(actual).toEqual(expected);

    });

    it("should be able to update a user", function() {

        var expected = new User({
            username: "expected user",
            password: "Shhhh",
            name: "expect name",
            role: "Admin"
        });

        new User({
            username: "user in db",
            password: "Shhhh",
            name: "name in db",
            role: "Admin"
        }).save(function(err) {
            console.log(err)
        });

        cmd.Update(
            expected.username,
            expected.password,
            expected.name,
            expected.role);

        var actual = User.find({}, function(err, found) {
            console.log(found);
            return found;
        });

        expect(actual).toEqual(expected);

    });

    it("can read", function() {


        var actual = cmd.CanRead();

        expect(actual).toBeTruthy();

    });

    it("can delete", function() {


        var actual = cmd.CanDelete();

        expect(actual).toBeTruthy();

    });

    it("can update", function() {


        var actual = cmd.CanUpdate();

        expect(actual).toBeTruthy();

    });

    it("can create", function() {


        var actual = cmd.CanCreate();

        expect(actual).toBeTruthy();

    });

});