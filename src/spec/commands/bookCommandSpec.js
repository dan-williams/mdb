describe("bookCommands", function() {
  var Book = require('../../models/bookSchema.js');
  var cmd = require('../../commands/book');
  var helpers = require('../helpers');

  beforeEach(function() {
    helpers.ClearSchema(Book);
  });


  it("should be able to create a book", function() {

    var expected = new Book({
      name: "bookName",
      author: "bookAuthor",
      genre: "horror",
      description: "bookDesc"
    });

    cmd.Create(
      expected.name,
      expected.author,
      expected.genre,
      expected.description);

    var actual = Book.find({}, function(err, found) {
      console.log(found);
      return found;
    });

    expect(actual).toEqual(expected);

  });

  it("should be able to update a book", function() {

    var expected = new Book({
      name: "bookName",
      author: "bookAuthor",
      genre: "horror",
      description: "bookDesc"
    });

    new Book({
      name: "bookName",
      author: "author of book in db",
      genre: "comedy",
      description: "we being updated yo!"
    }).save(function(err) {
      console.log(err)
    });

    cmd.Update(
      expected.name,
      expected.author,
      expected.genre,
      expected.description);

    var actual = Book.find({}, function(err, found) {
      console.log(found);
      return found;
    });

    expect(actual).toEqual(expected);

  });

  it("can read", function() {


    var actual = cmd.CanRead();

    expect(actual).toBeTruthy();

  });

  it("can delete", function() {


    var actual = cmd.CanDelete();

    expect(actual).toBeTruthy();

  });

  it("can update", function() {


    var actual = cmd.CanUpdate();

    expect(actual).toBeTruthy();

  });

  it("can create", function() {


    var actual = cmd.CanCreate();

    expect(actual).toBeTruthy();

  });
  
});