"use strict";

(function (helpers) {
  var dbHelpers = require('./dbHelpers.js')
  var NewDbHelpers = new dbHelpers()

  helpers.ClearSchema = function (Schema) {
    NewDbHelpers.ClearSchema(Schema);
  }

})(module.exports)
