var app = require('../../server.js');
var request = require('supertest')(app);
var Book = require('../../models/bookSchema.js');

describe('Book Controller Spec', function() {

    beforeEach(function() {
        helpers.ClearSchema(Book);
    });

    it('GET /books', function(done) {
        request
            .get('/books')
            .expect(201)
            .expect('Content-Type', 'application/html; charset=utf-8')
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it('GET /books/create', function(done) {
        request
            .get('/books/create')
            .expect(201)
            .expect('Content-Type', 'application/html; charset=utf-8')
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it('POST /books/create', function(done) {
        var book = new Book({
            name: "bookName",
            author: "bookAuthor",
            genre: "horror",
            description: "bookDesc"
        });

        request
            .post('/books/create')
            .send(book)
            .expect(200)
        done();
    });

    it('GET /books/update/:name', function(done) {
        var book = new Book({
            name: "bookName",
            author: "bookAuthor",
            genre: "horror",
            description: "bookDesc"
        }).save();

        request
            .get('/books/update/' + book.name)
            .expect(201)
            .expect('Content-Type', 'application/html; charset=utf-8')
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it('POST /books/update/:name', function(done) {
        var book = new Book({
            name: "bookName",
            author: "bookAuthor",
            genre: "horror",
            description: "bookDesc"
        });

        request
            .post('/books/update/' + book.name)
            .send(book)
            .expect(200)
        done();
    });

    it('GET /books/delete/:name', function(done) {
        var book = new Book({
            name: "bookName",
            author: "bookAuthor",
            genre: "horror",
            description: "bookDesc"
        }).save();

        request
            .get('/books/delete/' + book.name)
            .expect(201)
            .expect('Content-Type', 'application/html; charset=utf-8')
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

});