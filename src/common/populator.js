'use strict'

var BookCreate = require('../commands/book/create.js')
var BookCreateCommand = new BookCreate()
var UserCreate = require('../commands/user/create.js')
var UserCreateCommand = new UserCreate()
var log = require('../common/logger.js')
var Logger = new log()

function Populator () {
}

Populator.prototype.Populate = function (db) {
  try {
    Logger.Info('Droping current collection')
    Populator.prototype.Populate = function (db) {
      db.collections['mdb'].drop(function (err) {
        Logger.Info('collection dropped!')
        if (err) {
          Logger.Error(err)
        }
      })
    }
  } catch(error) {
    if (error) {
      Logger.Error(error)
    }
  }
  finally {
    Logger.Info('Populating Collection!')

    Logger.Info('Populating Users')
    UserCreateCommand.Execute('AdminUser', 'Admin', 'Admin', 'Admin')
    UserCreateCommand.Execute('StandardUser', 'User', 'User', 'User')

    Logger.Info('Populating Books')
    BookCreateCommand.Execute('HorrorBook', 'TestAuthor', 'Horror', 'Dummy Horror Book')
    BookCreateCommand.Execute('Sci-FiBook', 'TestAuthor', 'Sci-Fi', 'Dummy Sci-Fi Book')
    BookCreateCommand.Execute('CrimeBook', 'TestAuthor', 'Crime', 'Dummy Crime Book')
    BookCreateCommand.Execute('BiographyBook', 'TestAuthor', 'Biography', 'Dummy Biography Book')
  }
}

module.exports = Populator
