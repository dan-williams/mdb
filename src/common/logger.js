'use strict'

var winston = require('winston')
var chalk = require('chalk')

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.File)({filename: 'Main.log'})
  ]
})

function Logger () {
}

Logger.prototype.Debug = function (msg) {
  var debug = chalk.bold.yellow
  logger.debug(msg)
  console.log(debug(msg))
}

Logger.prototype.Info = function (msg) {
  var info = chalk.bold.green
  logger.info(msg)
  console.log(info(msg))
}

Logger.prototype.Warn = function (msg) {
  var warn = chalk.bold.orange
  logger.warn(msg)
  console.log(warn(msg))
}

Logger.prototype.Error = function (msg) {
  var error = chalk.bold.red
  logger.error(msg)
  console.log(error(msg))
}

Logger.prototype.Fatal = function (msg) {
  var fatal = chalk.bold.red
  logger.log('verbose', msg)
  console.log(fatal(msg))
}

module.exports = Logger
