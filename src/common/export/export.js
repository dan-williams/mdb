
var nodeExcel = require('excel-export');

function Exporter() {
}

Exporter.prototype.excel = function (res, columnsToRender, rowsToRender){
  	var conf ={};
	conf.stylesXmlFile = "styles.xml";
    conf.name = "mysheet";
  	conf.cols = columnsToRender

  	conf.rows = rowsToRender;

  	var result = nodeExcel.execute(conf);
  	res.setHeader('Content-Type', 'application/vnd.openxmlformats');
  	res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
  	res.end(result, 'binary');
}

module.exports = Exporter
