"use strict";

(function(exportFile) {
    var exp = require("./export.js");
    var cmd = new exp();

    exportFile.Excel = function(res, columnsToRender, rowsToRender){
        cmd.excel(res, columnsToRender, rowsToRender)
    }

})(module.exports);
