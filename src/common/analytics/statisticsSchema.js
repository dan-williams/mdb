'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var statisticsSchema = new Schema({
  route: String,
  request: String,
  user_id: String,
  user_username: String,
  user_name: String,
  user_role: String,
  dateRequested: {type: Date, default: Date.now}
})

var Statistics = mongoose.model('Statistics', statisticsSchema)

module.exports = Statistics
