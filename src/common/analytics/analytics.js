'use strict'

var stats = require('./statisticsSchema')
var log = require('../../common/logger.js')
var Logger = new log()

function Analytics() {
}

Analytics.prototype.Add = function (Route, Request, User) {
  var analyticsObject = new stats({
     route: Route,
  request: Request,
  user_id : User._id,
  user_username: User.username,
  user_name : User.name,
  user_role: User.role
  })
  analyticsObject.save(function (err) {
    Logger.Error(err)
  })
}

module.exports = Analytics
