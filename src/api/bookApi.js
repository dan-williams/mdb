'use strict'

var book = require('../models/bookSchema')

module.exports = function (app, requireLogin) {

  app.get('/api/books', requireLogin, function (req, res) {
    book.find({}, function (err, found) {
      res.json(found);
    })
  })
}
