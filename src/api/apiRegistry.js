'use strict'

var log = require('../common/logger.js')
var Logger = new log()

module.exports = function (app) {

  function requireLogin (req, res, next) {
    if (!req.user) {
      res.json({message : 'Permission Denied!'})
    } else {
      next()
    }
  }

  require('./reportApi.js')(app, requireLogin)
  require('./bookApi.js')(app, requireLogin)
  require('./userApi.js')(app, requireLogin)

}
