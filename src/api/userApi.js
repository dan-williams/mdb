'use strict'

var user = require('../models/userSchema')

module.exports = function (app, requireLogin) {

  app.get('/api/users', requireLogin, function (req, res) {
    user.find({}, function (err, found) {
      res.json(found);
    })
  })
}
