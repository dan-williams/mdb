'use strict'

var stats = require('../common/analytics/statisticsSchema.js')

module.exports = function (app, requireLogin) {

  app.get('/api/stats', requireLogin, function (req, res) {
    stats.find({}, function (err, found) {
      res.json(found);
    })
  })
}
